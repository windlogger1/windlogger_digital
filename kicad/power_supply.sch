EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Windlogger digital"
Date "2020-02-21"
Rev "1.2.1"
Comp "ALEEA"
Comment1 "LONGUET Gilles"
Comment2 "AGPL3"
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1550 1575 0    60   Input ~ 0
UDC
Text HLabel 1125 2400 0    60   Input ~ 0
Uac_hi
Text Notes 4250 1550 0    60   ~ 0
DC-DC converter\nVin 8 to 40V\nVout 5V
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR032
U 1 1 5886D48F
P 8100 5125
F 0 "#PWR032" H 8100 4975 50  0001 C CNN
F 1 "+5V" H 8100 5265 50  0000 C CNN
F 2 "" H 8100 5125 50  0000 C CNN
F 3 "" H 8100 5125 50  0000 C CNN
	1    8100 5125
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:CP-Device-digital-rescue C15
U 1 1 5886E707
P 9375 5350
F 0 "C15" H 9400 5450 50  0000 L CNN
F 1 "120u" H 9400 5250 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 9413 5200 50  0001 C CNN
F 3 "" H 9375 5350 50  0000 C CNN
	1    9375 5350
	1    0    0    -1  
$EndComp
Text Notes 7825 4775 0    60   ~ 0
5 to 4.1V, 2 A, mount it when use the GPRS sim 800L module
$Comp
L digital-rescue:+BATT-power-digital-rescue #PWR035
U 1 1 588751D0
P 9800 5050
F 0 "#PWR035" H 9800 4900 50  0001 C CNN
F 1 "+BATT" H 9675 5175 50  0000 C CNN
F 2 "" H 9800 5050 50  0000 C CNN
F 3 "" H 9800 5050 50  0000 C CNN
	1    9800 5050
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:LM358-Amplifier_Operational-digital-rescue U2
U 1 1 58883066
P 4875 5225
F 0 "U2" H 4825 5425 50  0000 L CNN
F 1 "LM358" H 4825 4975 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4875 5225 50  0001 C CNN
F 3 "" H 4875 5225 50  0000 C CNN
	1    4875 5225
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:LM358-Amplifier_Operational-digital-rescue U2
U 2 1 588830EE
P 5825 5325
F 0 "U2" H 5775 5525 50  0000 L CNN
F 1 "LM358" H 5775 5075 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5825 5325 50  0001 C CNN
F 3 "" H 5825 5325 50  0000 C CNN
	2    5825 5325
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR027
U 1 1 58883DD7
P 4150 6475
F 0 "#PWR027" H 4150 6325 50  0001 C CNN
F 1 "+5V" H 4150 6615 50  0000 C CNN
F 2 "" H 4150 6475 50  0000 C CNN
F 3 "" H 4150 6475 50  0000 C CNN
	1    4150 6475
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+3.3V-power-digital-rescue #PWR029
U 1 1 58884C23
P 4425 5325
F 0 "#PWR029" H 4425 5175 50  0001 C CNN
F 1 "+3.3V" H 4425 5465 50  0000 C CNN
F 2 "" H 4425 5325 50  0000 C CNN
F 3 "" H 4425 5325 50  0000 C CNN
	1    4425 5325
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:R-Device-digital-rescue R4
U 1 1 58884FF8
P 4050 4900
F 0 "R4" V 4130 4900 50  0000 C CNN
F 1 "10k" V 4050 4900 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3980 4900 50  0001 C CNN
F 3 "" H 4050 4900 50  0000 C CNN
	1    4050 4900
	1    0    0    -1  
$EndComp
Text Label 3875 4675 0    60   ~ 0
Vin
$Comp
L digital-rescue:C-Device-digital-rescue C8
U 1 1 588853C8
P 3650 5375
F 0 "C8" H 3675 5475 50  0000 L CNN
F 1 "100n" H 3675 5275 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3688 5225 50  0001 C CNN
F 3 "" H 3650 5375 50  0000 C CNN
	1    3650 5375
	1    0    0    -1  
$EndComp
Text GLabel 5725 6400 0    60   Input ~ 0
USBVCC
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR031
U 1 1 588869CB
P 6725 6175
F 0 "#PWR031" H 6725 6025 50  0001 C CNN
F 1 "+5V" H 6725 6315 50  0000 C CNN
F 2 "" H 6725 6175 50  0000 C CNN
F 3 "" H 6725 6175 50  0000 C CNN
	1    6725 6175
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:C-Device-digital-rescue C18
U 1 1 58887D5F
P 9800 2200
F 0 "C18" H 9825 2300 50  0000 L CNN
F 1 "100n" H 9825 2100 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 9838 2050 50  0001 C CNN
F 3 "" H 9800 2200 50  0000 C CNN
	1    9800 2200
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:CP-Device-digital-rescue C14
U 1 1 5879D4DC
P 9075 5350
F 0 "C14" H 9100 5450 50  0000 L CNN
F 1 "120u" H 9100 5250 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 9113 5200 50  0001 C CNN
F 3 "" H 9075 5350 50  0000 C CNN
	1    9075 5350
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:C-Device-digital-rescue C9
U 1 1 587F8D8D
P 3700 7025
F 0 "C9" H 3725 7125 50  0000 L CNN
F 1 "100n" H 3725 6925 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3738 6875 50  0001 C CNN
F 3 "" H 3700 7025 50  0000 C CNN
	1    3700 7025
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:D-Device-digital-rescue D6
U 1 1 58839433
P 8500 5125
F 0 "D6" H 8500 5225 50  0000 C CNN
F 1 "SR5100" H 8500 5025 50  0000 C CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 8500 5125 50  0001 C CNN
F 3 "" H 8500 5125 50  0000 C CNN
	1    8500 5125
	-1   0    0    1   
$EndComp
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG07
U 1 1 5883D1ED
P 10025 5100
F 0 "#FLG07" H 10025 5195 50  0001 C CNN
F 1 "PWR_FLAG" H 10125 5275 50  0000 C CNN
F 2 "" H 10025 5100 50  0000 C CNN
F 3 "" H 10025 5100 50  0000 C CNN
	1    10025 5100
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:Q_PMOS_GDSD-Device-digital-rescue Q1
U 1 1 58B4BB1B
P 6150 6300
F 0 "Q1" V 6050 6450 50  0000 R CNN
F 1 "NDT2955" V 6375 6375 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 6350 6400 50  0001 C CNN
F 3 "" H 6150 6300 50  0000 C CNN
	1    6150 6300
	0    -1   1    0   
$EndComp
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG02
U 1 1 58B5D8E8
P 2325 7025
F 0 "#FLG02" H 2325 7120 50  0001 C CNN
F 1 "PWR_FLAG" H 2325 7205 50  0000 C CNN
F 2 "" H 2325 7025 50  0000 C CNN
F 3 "" H 2325 7025 50  0000 C CNN
	1    2325 7025
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:LM2672M-5.0-Regulator_Switching-digital-rescue U3
U 1 1 58B7448A
P 4650 2225
F 0 "U3" H 4250 2675 50  0000 L CNN
F 1 "LM2672M-5.0" H 4250 2575 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5000 1875 50  0001 C CIN
F 3 "" H 4650 2225 50  0000 C CNN
	1    4650 2225
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:CP-Device-digital-rescue C10
U 1 1 58B7472E
P 3800 2750
F 0 "C10" H 3825 2850 50  0000 L CNN
F 1 "47u" H 3825 2650 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 3838 2600 50  0001 C CNN
F 3 "" H 3800 2750 50  0000 C CNN
	1    3800 2750
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:D_Schottky-Device-digital-rescue D5
U 1 1 58B74F4B
P 5425 2925
F 0 "D5" H 5425 3025 50  0000 C CNN
F 1 "SR5100" H 5425 2825 50  0000 C CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 5425 2925 50  0001 C CNN
F 3 "" H 5425 2925 50  0000 C CNN
	1    5425 2925
	0    1    1    0   
$EndComp
$Comp
L digital-rescue:INDUCTOR_SMALL-digital L1
U 1 1 58B753FD
P 5750 2425
F 0 "L1" H 5750 2525 50  0000 C CNN
F 1 "47u" H 5750 2375 50  0000 C CNN
F 2 "windlog:inductance_murata_1200LRS" H 5750 2425 50  0001 C CNN
F 3 "" H 5750 2425 50  0000 C CNN
	1    5750 2425
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:CP-Device-digital-rescue C12
U 1 1 58B758EF
P 6250 2925
F 0 "C12" H 6275 3025 50  0000 L CNN
F 1 "120u" H 6275 2825 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 6288 2775 50  0001 C CNN
F 3 "" H 6250 2925 50  0000 C CNN
	1    6250 2925
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:C_Small-Device-digital-rescue C11
U 1 1 58B75C79
P 5300 2225
F 0 "C11" H 5310 2295 50  0000 L CNN
F 1 "10n" H 5310 2145 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5300 2225 50  0001 C CNN
F 3 "" H 5300 2225 50  0000 C CNN
	1    5300 2225
	0    1    1    0   
$EndComp
Text Label 2050 7475 2    60   ~ 0
Vin
Wire Notes Line
	2625 3950 2600 3950
Wire Wire Line
	9375 5500 9375 5600
Wire Wire Line
	9375 5125 9375 5200
Connection ~ 9375 5125
Wire Wire Line
	7975 1950 8450 1950
Wire Wire Line
	750  7075 1325 7075
Wire Wire Line
	9800 1950 9800 2050
Connection ~ 9800 1950
Wire Wire Line
	9500 2050 9500 1950
Connection ~ 9500 1950
Wire Wire Line
	9800 2425 9800 2350
Wire Wire Line
	9500 2425 9500 2350
Connection ~ 9500 2425
Wire Wire Line
	8950 2150 8950 2425
Connection ~ 8950 2425
Wire Wire Line
	5175 5225 5525 5225
Wire Wire Line
	5525 5425 5525 5900
Wire Wire Line
	5525 5900 6150 5900
Wire Wire Line
	4425 5325 4575 5325
Wire Wire Line
	3350 5125 3650 5125
Wire Wire Line
	4050 4750 4050 4675
Wire Wire Line
	4050 4675 3875 4675
Wire Wire Line
	3350 5650 3650 5650
Wire Wire Line
	6350 6400 6725 6400
Wire Wire Line
	5725 6400 5875 6400
Connection ~ 9375 5600
Wire Wire Line
	9075 5500 9075 5600
Wire Wire Line
	9075 5200 9075 5125
Connection ~ 9075 5125
Wire Wire Line
	8450 2425 8950 2425
Wire Wire Line
	4150 6550 4150 6475
Wire Wire Line
	3700 7175 3700 7250
Wire Wire Line
	8350 5125 8100 5125
Wire Wire Line
	8650 5125 9075 5125
Wire Wire Line
	9075 5600 9375 5600
Wire Wire Line
	10025 5125 10025 5100
Wire Wire Line
	5950 6500 5875 6500
Wire Wire Line
	5875 6500 5875 6400
Connection ~ 5875 6400
Wire Wire Line
	1625 7075 1975 7075
Wire Wire Line
	1975 7075 1975 7000
Wire Wire Line
	2325 7075 2325 7025
Connection ~ 1975 7075
Wire Wire Line
	2600 2025 2925 2025
Wire Wire Line
	5150 2425 5425 2425
Wire Wire Line
	5425 2225 5425 2425
Connection ~ 5425 2425
Wire Wire Line
	6000 2425 6050 2425
Wire Wire Line
	6250 2425 6250 2775
Wire Wire Line
	5150 2225 5200 2225
Wire Wire Line
	5400 2225 5425 2225
Wire Wire Line
	6050 2025 6050 2425
Connection ~ 6050 2425
Connection ~ 6250 2425
Connection ~ 6150 5900
Wire Wire Line
	6150 5325 6150 5900
Wire Wire Line
	6125 5325 6150 5325
$Comp
L digital-rescue:BZX84Cxx-Diode-digital-rescue D4
U 1 1 58B78735
P 3350 5425
F 0 "D4" H 3350 5525 50  0000 C CNN
F 1 "BZX79-C4V7,113" H 3350 5325 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 3350 5425 50  0001 C CNN
F 3 "" H 3350 5425 50  0000 C CNN
	1    3350 5425
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 5225 3650 5125
Connection ~ 3650 5125
$Comp
L digital-rescue:CP-Device-digital-rescue C16
U 1 1 5887130F
P 9500 2200
F 0 "C16" H 9525 2300 50  0000 L CNN
F 1 "1u" H 9525 2100 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 9538 2050 50  0001 C CNN
F 3 "" H 9500 2200 50  0000 C CNN
	1    9500 2200
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:CP-Device-digital-rescue C13
U 1 1 58B7959F
P 8450 2200
F 0 "C13" H 8475 2300 50  0000 L CNN
F 1 "1u" H 8475 2100 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 8488 2050 50  0001 C CNN
F 3 "" H 8450 2200 50  0000 C CNN
	1    8450 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2050 8450 1950
Connection ~ 8450 1950
Wire Wire Line
	8450 2425 8450 2350
Wire Wire Line
	9250 1950 9500 1950
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG03
U 1 1 58B7A840
P 2350 6500
F 0 "#FLG03" H 2350 6595 50  0001 C CNN
F 1 "PWR_FLAG" H 2350 6680 50  0000 C CNN
F 2 "" H 2350 6500 50  0000 C CNN
F 3 "" H 2350 6500 50  0000 C CNN
	1    2350 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	775  6550 1325 6550
Wire Wire Line
	2000 6550 2000 6475
Wire Wire Line
	2350 6550 2350 6500
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR023
U 1 1 58B7A904
P 2000 6475
F 0 "#PWR023" H 2000 6325 50  0001 C CNN
F 1 "+5V" H 2000 6615 50  0000 C CNN
F 2 "" H 2000 6475 50  0000 C CNN
F 3 "" H 2000 6475 50  0000 C CNN
	1    2000 6475
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+3V3-power-digital-rescue #PWR022
U 1 1 58B7AC85
P 1975 7000
F 0 "#PWR022" H 1975 6850 50  0001 C CNN
F 1 "+3V3" H 1975 7140 50  0000 C CNN
F 2 "" H 1975 7000 50  0000 C CNN
F 3 "" H 1975 7000 50  0000 C CNN
	1    1975 7000
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+BATT-power-digital-rescue #PWR021
U 1 1 58B7BD01
P 1900 5275
F 0 "#PWR021" H 1900 5125 50  0001 C CNN
F 1 "+BATT" H 1900 5415 50  0000 C CNN
F 2 "" H 1900 5275 50  0000 C CNN
F 3 "" H 1900 5275 50  0000 C CNN
	1    1900 5275
	1    0    0    -1  
$EndComp
Text GLabel 1675 4900 2    60   Input ~ 0
USBVCC
$Comp
L digital-rescue:Conn_01x03-Connector_Generic-digital-rescue P12
U 1 1 58B7C006
P 2400 2025
F 0 "P12" H 2400 2225 50  0000 C CNN
F 1 "CONN_01X03" V 2500 2025 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 2400 2025 50  0001 C CNN
F 3 "" H 2400 2025 50  0000 C CNN
	1    2400 2025
	-1   0    0    1   
$EndComp
Wire Wire Line
	2600 1925 2650 1925
Wire Wire Line
	2650 1925 2650 1575
Wire Wire Line
	2650 1575 1550 1575
Wire Wire Line
	2600 2125 2650 2125
Wire Wire Line
	2650 2125 2650 2400
Wire Wire Line
	2650 2400 1125 2400
Text Notes 1200 1275 0    60   ~ 0
From shield PCB
Text Notes 2575 1425 0    60   ~ 0
Jumper to select :\nUDC or UAC source
Text Notes 8400 1625 0    60   ~ 0
DC linear regulator for 3.3V supply
Wire Notes Line
	500  4225 11225 4225
Wire Notes Line
	6975 500  6975 6525
Text Notes 3500 4500 0    60   ~ 0
Auto switch, if if Vin is set, don't use +5V from USB
Wire Notes Line
	2975 4250 2975 7800
Text Label 775  6550 0    60   ~ 0
+5V_PWR
Text Label 750  7075 0    60   ~ 0
+3V3_PWR
Wire Wire Line
	6050 2025 5150 2025
Text Notes 525  6100 0    60   ~ 0
DESCRIPTION: \nThese jumpers allow the isolation of the feeder.\nTo be used during the test phase.
Text Notes 575  5750 0    60   ~ 0
JUMPER CONNECTORS
Wire Notes Line
	475  5650 2975 5650
Wire Notes Line
	1550 5775 575  5775
Text Notes 600  4350 0    60   ~ 0
TESTING PINS
Text Notes 550  4650 0    60   ~ 0
VERY IMPORTANT: \nThis pins can be used to implement testing probes.
Wire Notes Line
	600  4375 1200 4375
$Comp
L digital-rescue:TestPoint-Connector-digital-rescue W1
U 1 1 58B8051A
P 1200 4775
F 0 "W1" V 1250 4825 50  0000 C CNN
F 1 "Vin" V 1200 5100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D2.54mm_Drill1.5mm_Beaded" H 1400 4775 50  0001 C CNN
F 3 "" H 1400 4775 50  0000 C CNN
	1    1200 4775
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:TestPoint-Connector-digital-rescue W2
U 1 1 58B80838
P 1200 4900
F 0 "W2" V 1250 4950 50  0000 C CNN
F 1 "USBVCC" V 1200 5225 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D2.54mm_Drill1.5mm_Beaded" H 1400 4900 50  0001 C CNN
F 3 "" H 1400 4900 50  0000 C CNN
	1    1200 4900
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:TestPoint-Connector-digital-rescue W3
U 1 1 58B808A8
P 1200 5025
F 0 "W3" V 1250 5075 50  0000 C CNN
F 1 "+5V" V 1200 5350 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D2.54mm_Drill1.5mm_Beaded" H 1400 5025 50  0001 C CNN
F 3 "" H 1400 5025 50  0000 C CNN
	1    1200 5025
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:TestPoint-Connector-digital-rescue W4
U 1 1 58B809EA
P 1200 5150
F 0 "W4" V 1250 5200 50  0000 C CNN
F 1 "+3.3V" V 1200 5475 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D2.54mm_Drill1.5mm_Beaded" H 1400 5150 50  0001 C CNN
F 3 "" H 1400 5150 50  0000 C CNN
	1    1200 5150
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:TestPoint-Connector-digital-rescue W5
U 1 1 58B809F0
P 1200 5275
F 0 "W5" V 1250 5325 50  0000 C CNN
F 1 "+BATT" V 1200 5600 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D2.54mm_Drill1.5mm_Beaded" H 1400 5275 50  0001 C CNN
F 3 "" H 1400 5275 50  0000 C CNN
	1    1200 5275
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1675 4900 1200 4900
Wire Wire Line
	1675 4775 1200 4775
Wire Wire Line
	1675 5025 1200 5025
Wire Wire Line
	1675 5150 1200 5150
Wire Wire Line
	1200 5275 1900 5275
$Comp
L digital-rescue:TestPoint-Connector-digital-rescue W6
U 1 1 58B82609
P 1200 5400
F 0 "W6" V 1250 5450 50  0000 C CNN
F 1 "GND" V 1200 5725 50  0000 C CNN
F 2 "TestPoint:TestPoint_Loop_D2.54mm_Drill1.5mm_Beaded" H 1400 5400 50  0001 C CNN
F 3 "" H 1400 5400 50  0000 C CNN
	1    1200 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1375 5400 1200 5400
$Comp
L digital-rescue:GND-power-digital-rescue #PWR020
U 1 1 58B82655
P 1375 5400
F 0 "#PWR020" H 1375 5150 50  0001 C CNN
F 1 "GND" H 1375 5250 50  0000 C CNN
F 2 "" H 1375 5400 50  0000 C CNN
F 3 "" H 1375 5400 50  0000 C CNN
	1    1375 5400
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:D_Schottky-Device-digital-rescue D3
U 1 1 58B82C46
P 3075 2025
F 0 "D3" H 3075 2125 50  0000 C CNN
F 1 "SR5100" H 3075 1925 50  0000 C CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 3075 2025 50  0001 C CNN
F 3 "" H 3075 2025 50  0000 C CNN
	1    3075 2025
	-1   0    0    1   
$EndComp
NoConn ~ 4150 2225
NoConn ~ 4150 2325
NoConn ~ 4150 2425
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG04
U 1 1 58B8AAEC
P 3425 2000
F 0 "#FLG04" H 3425 2095 50  0001 C CNN
F 1 "PWR_FLAG" H 3425 2180 50  0000 C CNN
F 2 "" H 3425 2000 50  0000 C CNN
F 3 "" H 3425 2000 50  0000 C CNN
	1    3425 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 2000 3425 2025
$Comp
L digital-rescue:GND-power-digital-rescue #PWR034
U 1 1 58B8C169
P 9375 5675
F 0 "#PWR034" H 9375 5425 50  0001 C CNN
F 1 "GND" H 9375 5525 50  0000 C CNN
F 2 "" H 9375 5675 50  0000 C CNN
F 3 "" H 9375 5675 50  0000 C CNN
	1    9375 5675
	1    0    0    -1  
$EndComp
Text Label 6675 2425 2    60   ~ 0
+5V_PWR
Text Label 7975 1950 0    60   ~ 0
+5V_PWR
Text Label 10325 1950 2    60   ~ 0
+3V3_PWR
Text Label 1675 5025 2    60   ~ 0
+5V_PWR
Text Label 1675 5150 2    60   ~ 0
+3V3_PWR
$Comp
L digital-rescue:GND-power-digital-rescue #PWR024
U 1 1 58B8D5B6
P 3650 5875
F 0 "#PWR024" H 3650 5625 50  0001 C CNN
F 1 "GND" H 3650 5725 50  0000 C CNN
F 2 "" H 3650 5875 50  0000 C CNN
F 3 "" H 3650 5875 50  0000 C CNN
	1    3650 5875
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR028
U 1 1 58B8D6AB
P 4150 7250
F 0 "#PWR028" H 4150 7000 50  0001 C CNN
F 1 "GND" H 4150 7100 50  0000 C CNN
F 2 "" H 4150 7250 50  0000 C CNN
F 3 "" H 4150 7250 50  0000 C CNN
	1    4150 7250
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG05
U 1 1 58B9B6B2
P 6800 2400
F 0 "#FLG05" H 6800 2495 50  0001 C CNN
F 1 "PWR_FLAG" H 6800 2580 50  0000 C CNN
F 2 "" H 6800 2400 50  0000 C CNN
F 3 "" H 6800 2400 50  0000 C CNN
	1    6800 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2425 6800 2400
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG06
U 1 1 58BA0E3C
P 10000 5575
F 0 "#FLG06" H 10000 5670 50  0001 C CNN
F 1 "PWR_FLAG" H 10000 5755 50  0000 C CNN
F 2 "" H 10000 5575 50  0000 C CNN
F 3 "" H 10000 5575 50  0000 C CNN
	1    10000 5575
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 5600 10000 5575
$Comp
L digital-rescue:C-Device-digital-rescue C17
U 1 1 58BA125E
P 9675 5350
F 0 "C17" H 9700 5450 50  0000 L CNN
F 1 "100n" H 9700 5250 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 9713 5200 50  0001 C CNN
F 3 "" H 9675 5350 50  0000 C CNN
	1    9675 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9675 5200 9675 5125
Connection ~ 9675 5125
Wire Wire Line
	9675 5500 9675 5600
Connection ~ 9675 5600
$Comp
L digital-rescue:MCP1700-digital U4
U 1 1 58DC18B8
P 8950 1950
F 0 "U4" H 9050 1800 50  0000 C CNN
F 1 "MCP1700" H 8950 2100 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3_Handsoldering" H 8950 1950 50  0001 C CNN
F 3 "" H 8950 1950 50  0000 C CNN
	1    8950 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 7475 2050 7475
Wire Wire Line
	1375 7475 925  7475
Text Label 925  7475 0    60   ~ 0
Vin_PWR
Text Label 3575 2025 0    60   ~ 0
Vin_PWR
Wire Wire Line
	4050 5050 4050 5125
Connection ~ 4050 5125
Text Label 1675 4775 2    60   ~ 0
Vin_PWR
Wire Wire Line
	9800 1950 10325 1950
Wire Wire Line
	9500 1950 9800 1950
Wire Wire Line
	9500 2425 9800 2425
Wire Wire Line
	8950 2425 8950 2500
Wire Wire Line
	8950 2425 9500 2425
Wire Wire Line
	9375 5600 9375 5675
Wire Wire Line
	9375 5600 9675 5600
Wire Wire Line
	9075 5125 9375 5125
Wire Wire Line
	5875 6400 5950 6400
Wire Wire Line
	1975 7075 2325 7075
Wire Wire Line
	5425 2425 5500 2425
Wire Wire Line
	5425 2425 5425 2775
Wire Wire Line
	6050 2425 6250 2425
Wire Wire Line
	6250 2425 6800 2425
Wire Wire Line
	6150 5900 6150 6100
Wire Wire Line
	3650 5125 4050 5125
Wire Wire Line
	8450 1950 8650 1950
Wire Wire Line
	2000 6550 2350 6550
Wire Wire Line
	9675 5125 9800 5125
Wire Wire Line
	9675 5600 10000 5600
Wire Wire Line
	4050 5125 4575 5125
Wire Wire Line
	3350 5275 3350 5125
Wire Wire Line
	3350 5650 3350 5575
$Comp
L digital-rescue:GND-power-digital-rescue #PWR033
U 1 1 5B9BA9E6
P 8950 2500
F 0 "#PWR033" H 8950 2250 50  0001 C CNN
F 1 "GND" H 8955 2327 50  0000 C CNN
F 2 "" H 8950 2500 50  0001 C CNN
F 3 "" H 8950 2500 50  0001 C CNN
	1    8950 2500
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR030
U 1 1 5B9BE51E
P 5425 3250
F 0 "#PWR030" H 5425 3000 50  0001 C CNN
F 1 "GND" H 5430 3077 50  0000 C CNN
F 2 "" H 5425 3250 50  0001 C CNN
F 3 "" H 5425 3250 50  0001 C CNN
	1    5425 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 3075 5425 3150
Wire Wire Line
	4650 2625 4650 3150
Wire Wire Line
	6250 3075 6250 3150
Wire Wire Line
	3225 2025 3425 2025
Connection ~ 3425 2025
Text Notes 8000 2800 0    60   ~ 0
changer l'empreinte pour du gros cms SOT 89\n
Connection ~ 4650 3150
Wire Wire Line
	4650 3150 5425 3150
Connection ~ 5425 3150
Wire Wire Line
	5425 3150 5425 3250
Wire Wire Line
	5425 3150 6250 3150
Wire Wire Line
	6725 6175 6725 6400
Wire Wire Line
	3650 5650 3650 5875
Wire Wire Line
	3650 5525 3650 5650
Connection ~ 3650 5650
$Comp
L digital-rescue:SolderJumper_2_Open-Jumper-digital-rescue JP1
U 1 1 5BA6EDEA
P 1475 6550
F 0 "JP1" H 1475 6755 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1475 6664 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1475 6550 50  0001 C CNN
F 3 "~" H 1475 6550 50  0001 C CNN
	1    1475 6550
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:CP-Device-digital-rescue C7
U 1 1 5BB59267
P 3200 2750
F 0 "C7" H 3225 2850 50  0000 L CNN
F 1 "47u" H 3225 2650 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 3238 2600 50  0001 C CNN
F 3 "" H 3200 2750 50  0000 C CNN
	1    3200 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 2025 3500 2025
Wire Wire Line
	3200 2600 3200 2500
Wire Wire Line
	3200 2500 3500 2500
Wire Wire Line
	3800 2500 3800 2600
Wire Wire Line
	3800 2900 3800 3000
Wire Wire Line
	3800 3000 3500 3000
Wire Wire Line
	3200 3000 3200 2900
Wire Wire Line
	3500 3000 3500 3150
Wire Wire Line
	3500 3150 4650 3150
Connection ~ 3500 3000
Wire Wire Line
	3500 3000 3200 3000
Wire Wire Line
	3500 2500 3500 2025
Connection ~ 3500 2500
Wire Wire Line
	3500 2500 3800 2500
Connection ~ 3500 2025
Wire Wire Line
	3500 2025 4150 2025
Text Notes 2750 3400 0    60   ~ 0
rs 707-5764 +ou- 20% 50V
$Comp
L digital-rescue:SolderJumper_2_Open-Jumper-digital-rescue JP2
U 1 1 5BB76AC4
P 1475 7075
F 0 "JP2" H 1475 7280 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1475 7189 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1475 7075 50  0001 C CNN
F 3 "~" H 1475 7075 50  0001 C CNN
	1    1475 7075
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:SolderJumper_2_Open-Jumper-digital-rescue JP3
U 1 1 5BB76B3C
P 1525 7475
F 0 "JP3" H 1525 7680 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1525 7589 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1525 7475 50  0001 C CNN
F 3 "~" H 1525 7475 50  0001 C CNN
	1    1525 7475
	1    0    0    -1  
$EndComp
Connection ~ 2000 6550
Wire Wire Line
	1625 6550 2000 6550
$Comp
L digital-rescue:LM358-Amplifier_Operational-digital-rescue U2
U 3 1 5BBA934E
P 4250 6850
F 0 "U2" H 4208 6896 50  0000 L CNN
F 1 "LM358" H 4208 6805 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4250 6850 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 4250 6850 50  0001 C CNN
	3    4250 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 7250 4150 7150
$Comp
L digital-rescue:GND-power-digital-rescue #PWR026
U 1 1 5BBB6D7C
P 3700 7250
F 0 "#PWR026" H 3700 7000 50  0001 C CNN
F 1 "GND" H 3700 7100 50  0000 C CNN
F 2 "" H 3700 7250 50  0000 C CNN
F 3 "" H 3700 7250 50  0000 C CNN
	1    3700 7250
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR025
U 1 1 5BBB6DFE
P 3700 6775
F 0 "#PWR025" H 3700 6625 50  0001 C CNN
F 1 "+5V" H 3700 6915 50  0000 C CNN
F 2 "" H 3700 6775 50  0000 C CNN
F 3 "" H 3700 6775 50  0000 C CNN
	1    3700 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 6775 3700 6875
Wire Wire Line
	9375 5125 9675 5125
Wire Wire Line
	9800 5050 9800 5125
Connection ~ 9800 5125
Wire Wire Line
	9800 5125 10025 5125
$EndSCHEMATC
